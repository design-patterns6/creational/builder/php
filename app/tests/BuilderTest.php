<?php

declare(strict_types=1);

namespace Builder\Tests;

use Builder\HTMLPageBuilder;
use Builder\HTMLPageDirector;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{
    public function testHTMLPageDirectorShouldReturnHtmlTagsWithTestingTextInside(): void
    {
        // GIVEN
        $pageBuilder = new HTMLPageBuilder();
        $pageDirector = new HTMLPageDirector($pageBuilder);
        $pageDirector->buildPage();
        // WHEN
        $page = $pageDirector->getPage();
        // THEN
        self::assertEquals(
            expected: '<html><head><title>Testing the HTMLPage</title></head><body><h1>Testing the HTMLPage</h1>Testing, testing, testing!</body></html>',
            actual: $page->showPage()
        );
    }
}
