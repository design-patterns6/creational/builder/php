<?php

declare(strict_types=1);

namespace Builder;

class HTMLPage
{
    public function __construct(
        private ?string $page = null,
        private ?string $pageTitle = null,
        private ?string $pageHeading = null,
        private ?string $pageText = null,
    ) {
    }

    public function showPage(): string
    {
        return $this->page ?? '';
    }

    public function setTitle(string $titleIn): void
    {
        $this->pageTitle = $titleIn;
    }

    public function setHeading(string $headingIn): void
    {
        $this->pageHeading = $headingIn;
    }

    public function setText(string $textIn): void
    {
        $this->pageText .= $textIn;
    }

    public function formatPage(): void
    {
        $this->page = '<html>';
        $this->page .= '<head><title>'.$this->pageTitle.'</title></head>';
        $this->page .= '<body>';
        $this->page .= '<h1>'.$this->pageHeading.'</h1>';
        $this->page .= $this->pageText;
        $this->page .= '</body>';
        $this->page .= '</html>';
    }
}
