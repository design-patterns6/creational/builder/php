<?php

declare(strict_types=1);

namespace Builder;

abstract class AbstractPageDirector
{
    abstract public function __construct(AbstractPageBuilder $builderIn);

    abstract public function buildPage(): void;

    abstract public function getPage(): HTMLPage;
}
