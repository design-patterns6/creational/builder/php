<?php

declare(strict_types=1);

namespace Builder;

class HTMLPageDirector extends AbstractPageDirector
{
    public function __construct(
        private readonly AbstractPageBuilder $builder
    ) {
    }

    public function buildPage(): void
    {
        $this->builder->setTitle(titleIn: 'Testing the HTMLPage');
        $this->builder->setHeading(headingIn: 'Testing the HTMLPage');
        $this->builder->setText(textIn: 'Testing, testing, testing!');
        $this->builder->formatPage();
    }

    public function getPage(): HTMLPage
    {
        return $this->builder->getPage();
    }
}
