<?php

declare(strict_types=1);

namespace Builder;

class HTMLPageBuilder extends AbstractPageBuilder
{
    public function __construct(
        private readonly HTMLPage $page = new HTMLPage()
    ) {
    }

    public function setTitle(string $titleIn): void
    {
        $this->page->setTitle(titleIn: $titleIn);
    }

    public function setHeading(string $headingIn): void
    {
        $this->page->setHeading(headingIn: $headingIn);
    }

    public function setText(string $textIn): void
    {
        $this->page->setText(textIn: $textIn);
    }

    public function formatPage(): void
    {
        $this->page->formatPage();
    }

    public function getPage(): HTMLPage
    {
        return $this->page;
    }
}
