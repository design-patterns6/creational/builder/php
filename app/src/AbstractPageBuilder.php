<?php

declare(strict_types=1);

namespace Builder;

abstract class AbstractPageBuilder
{
    abstract public function getPage(): HTMLPage;
}
